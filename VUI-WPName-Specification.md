# VUI - Presentation Exchange v1.0

* Authors: 
* Status: draft
* Start date: 
* Last update: 

## Table of contents
* [Summary](#summary)
* [Assumptions](#assumptions)
* [Normative references](#normative-references)
* [Definitions](#definitions)
* [Data Model](#data-model)
* [Features](#features)
* [Protocols](#protocols)
* [Implementation Guidelines](#implementation-guidelines)